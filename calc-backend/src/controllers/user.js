const catchAsync = require("../../utils/catchAsync");
const db = require("../database");

exports.getOrCreateUser = catchAsync(async (req, res, next) => {
    let allowedOs = ["ios", "android"];
    console.log(req.body);
    try {
        if (!allowedOs.includes(req.body.os)) throw new Error("400-Bad-Request"); // throws bad request error if device is not ios or android
        if (req.body.id === undefined) throw new Error("401-Unauthorized"); // if id is not defined

        let { id } = req.body;
        //check if existing
        const { rows } = await db.query("SELECT ID FROM users WHERE ID = $1", [id]);

        if (rows.length > 0) return res.status(200).json({ user: { uuid: id } });

        // creates new user
        await db.query("INSERT INTO users(ID) VALUES ($1)", [id]);

        res.status(201).json({ user: { uuid: id } });
    } catch (e) {
        console.log(e);
        let errorObject = {
            code: 500,
            description: "An error occurred",
            name: "500-Internal-Server-Error",
        };
        switch (e?.message) {
            case "400-Bad-Request":
                errorObject.code = 400;
                errorObject.name = e.message;
                errorObject.description = "Invalid body parameter";
                break;
            case "401-Unauthorized":
                errorObject.code = 401;
                errorObject.name = e.message;
                errorObject.description = "Unauthorized";
                break;
        }
        res.status(errorObject.code).json(errorObject);
    }
});

exports.addTransaction = catchAsync(async (req, res, next) => {
    try {
        if (req.params.uid === undefined) throw new Error("401-Unauthorized");

        let { uid } = req.params;
        const { rows } = await db.query("SELECT ID FROM users WHERE ID = $1", [uid]);
        if (rows.length === 0) throw new Error("404-Not-Found"); // no user found with the query
        if (req.body.calculation_string === undefined) throw new Error("400-Bad-Request"); // no calculation string given

        let { calculation_string } = req.body;

        await db.query("INSERT INTO history(calculation_string, user_uuid) VALUES ($1, $2)", [calculation_string, uid]);

        res.status(201).send("OK");
    } catch (e) {
        console.log(e);
        let errorObject = {
            code: 500,
            description: "An error occurred",
            name: "500-Internal-Server-Error",
        };
        switch (e?.message) {
            case "400-Bad-Request":
                errorObject.code = 400;
                errorObject.name = e.message;
                errorObject.description = "Invalid body parameter";
                break;
            case "401-Unauthorized":
                errorObject.code = 401;
                errorObject.name = e.message;
                errorObject.description = "Unauthorized";
                break;
            case "404-Not-Found":
                errorObject.code = 404;
                errorObject.name = e.message;
                errorObject.description = "User not found";
                break;
        }
        res.status(errorObject.code).send(errorObject.description);
    }
});

exports.deleteTransaction = catchAsync(async (req, res, next) => {
    try {
        if (req.params.uid === undefined) throw new Error("401-Unauthorized");

        let { uid } = req.params;
        const { rows } = await db.query("SELECT ID FROM users WHERE ID = $1", [uid]);
        if (rows.length === 0) throw new Error("404-Not-Found"); // no user found with the query

        await db.query("DELETE FROM history WHERE user_uuid = $1", [uid]);
        res.status(204).send("Resource Deleted");
    } catch (e) {
        let errorObject = {
            code: 500,
            description: "An error occurred",
            name: "500-Internal-Server-Error",
        };
        switch (e?.message) {
            case "400-Bad-Request":
                errorObject.code = 400;
                errorObject.name = e.message;
                errorObject.description = "Invalid body parameter";
                break;
            case "401-Unauthorized":
                errorObject.code = 401;
                errorObject.name = e.message;
                errorObject.description = "Unauthorized";
                break;
            case "404-Not-Found":
                errorObject.code = 404;
                errorObject.name = e.message;
                errorObject.description = "User not found";
                break;
        }
        res.status(errorObject.code).send(errorObject.description);
    }
});

exports.getTransaction = catchAsync(async (req, res, next) => {
    try {
        if (req.params.uid === undefined) throw new Error("401-Unauthorized");
        let { uid } = req.params;
        let { rows } = await db.query("SELECT ID FROM users WHERE ID = $1", [uid]);
        if (rows.length === 0) throw new Error("404-Not-Found"); // no user found with the query

        let results = await db.query("SELECT calculation_string FROM history WHERE user_uuid = $1", [uid]);

        res.status(200).json({ results: results.rows });
    } catch (e) {
        let errorObject = {
            code: 500,
            description: "An error occurred",
            name: "500-Internal-Server-Error",
        };
        switch (e?.message) {
            case "400-Bad-Request":
                errorObject.code = 400;
                errorObject.name = e.message;
                errorObject.description = "Invalid body parameter";
                break;
            case "401-Unauthorized":
                errorObject.code = 401;
                errorObject.name = e.message;
                errorObject.description = "Unauthorized";
                break;
            case "404-Not-Found":
                errorObject.code = 404;
                errorObject.name = e.message;
                errorObject.description = "User not found";
                break;
        }
        res.status(errorObject.code).send(errorObject.description);
    }
});
