const express = require("express");
const serverless = require("serverless-http");
const app = express();
const userRoutes = require("./routes/user");

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

//app.use("/app/user", userRoutes);
app.use("/.netlify/functions/server/app/user", userRoutes);
module.exports = app;
module.exports.handler = serverless(app);
