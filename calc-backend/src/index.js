require("dotenv").config();
const app = require("./server");
app.listen(process.env.SV_PORT, () => console.log(`App listening on port ${process.env.SV_PORT}`));
