const { getOrCreateUser, getTransaction, addTransaction, deleteTransaction } = require("../controllers/user");
const express = require("express");
const router = express.Router();

router.route("/").post(getOrCreateUser);
router.route("/:uid/transaction").post(addTransaction);
router.route("/:uid/transaction").get(getTransaction);
router.route("/:uid/transaction").delete(deleteTransaction);

module.exports = router;
