import React, { useEffect, useState } from "react";
import axios from "axios";
import { Image, Pressable, ScrollView, Text, View } from "react-native";
// import { Platform } from "react-native";
// import DeviceInfo from "react-native-device-info";

import { styles } from "./styles/global";
import HistoryLog from "../components/HistoryLog";

const History = ({ navigation, route }) => {
    const [historyLogs, setHistoryLogs] = useState([]);
    const [user, setUser] = useState(route?.params?.user);
    const BASE_URL = "https://apptain-dizon.netlify.app/.netlify/functions/server/app/user";

    async function fetchData() {
        let { data } = await axios.post(`${BASE_URL}`, {
            // ON EXPO GO
            os: "ios",
            id: "123456-xyzwx-jdh123",
            // ON EXPO DEV BUILD
            // os: Platform.OS,
            // id: DeviceInfo.getUniqueIdSync(),
        });
        setUser(data.user);
    }

    useEffect(() => {
        async function fetchHistory() {
            let { data } = await axios.get(`${BASE_URL}/${user.uuid}/transaction`);
            setHistoryLogs(
                data.results.map((item) => {
                    let temp = item.calculation_string.split("=");
                    return { calcString: temp[0], answer: temp[1] || 0 };
                })
            );
        }
        if (user) fetchHistory();
        else fetchData();
    }, [user]);

    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <Text style={{ textAlign: "center", fontWeight: "700", color: "#fff", marginTop: 30, fontSize: 24 }}>
                    History
                </Text>

                {historyLogs.length === 0 ? (
                    <View
                        style={{
                            backgroundColor: "#3B3D43",
                            height: 164,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginHorizontal: 24,
                            marginTop: 8,
                            borderRadius: 15,
                        }}
                    >
                        <Text style={{ fontSize: 20, fontWeight: "600", color: "#FFF" }}>Empty!</Text>
                        <Text style={{ fontSize: 20, fontWeight: "600", color: "#FFF" }}>Do some calculations/</Text>
                    </View>
                ) : (
                    <>
                        <Pressable
                            style={{ position: "absolute", right: 24, top: 30 }}
                            onPress={async (e) => {
                                setHistoryLogs([]);
                                await axios.delete(`${BASE_URL}/${user.uuid}/transaction`);
                            }}
                        >
                            <Image source={require("../../assets/trash.png")} style={{ width: 21 }} />
                        </Pressable>
                        <ScrollView
                            style={{
                                height: "100%",
                                borderTopLeftRadius: 15,
                                borderTopRightRadius: 15,
                                marginHorizontal: 24,
                                marginTop: 8,
                            }}
                        >
                            {historyLogs.map((item, index) => (
                                <HistoryLog key={index} calcString={item.calcString} answer={item.answer} />
                            ))}
                        </ScrollView>
                    </>
                )}
            </View>
        </View>
    );
};

export default History;
