import React, { useState, useEffect } from "react";
import axios from "axios";
import { Image, StyleSheet, Text, View } from "react-native";
// import { Platform } from "react-native";
// import DeviceInfo from "react-native-device-info";
import { styles } from "./styles/global";
import CalcButton from "../components/CalcButton";
import calculate from "calculate-string";

const Calculator = ({ navigation }) => {
    const BASE_URL = "https://apptain-dizon.netlify.app/.netlify/functions/server/app/user";
    const [calcString, setCalcString] = useState("");
    const [user, setUser] = useState({});
    const [value, setValue] = useState("0");

    useEffect(() => {
        async function fetchData() {
            let { data } = await axios.post(`${BASE_URL}`, {
                // ON EXPO GO
                os: "ios",
                id: "123456-xyzwx-jdh123",
                // ON EXPO DEV BUILD
                // os: Platform.OS,
                // id: DeviceInfo.getUniqueIdSync(),
            });
            setUser(data.user);
        }

        fetchData();
    }, [navigation]);

    const onNumberPress = (num) => {
        if (calcString.substring(-1) !== "%") setCalcString(calcString + `${num}`);
    };

    useEffect(() => {
        let calculatedValue = calculate(calcString);

        if (!isNaN(calculatedValue)) setValue(calculatedValue.toLocaleString("en-US"));
    }, [calcString]);

    const onOperationPress = (op) => {
        if (calcString.length > 0 && calcString.substring(-1) !== " ") setCalcString(calcString + ` ${op} `);
    };

    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <Text
                    style={{
                        color: "#5B5E67",
                        height: 35,
                        fontSize: 35,
                        textAlign: "right",
                        marginHorizontal: 24,
                        marginTop: 16,
                    }}
                >
                    {calcString}
                </Text>
                <Text
                    style={{
                        color: "#FFF",
                        fontSize: 72,
                        textAlign: "right",
                        marginHorizontal: 24,
                        marginTop: 8,
                    }}
                >
                    {value}
                </Text>
                <View style={calcStyling.container}>
                    <CalcButton
                        title={"AC"}
                        color="gray"
                        onPress={() => {
                            setCalcString("");
                            setValue(0);
                        }}
                    />
                    <CalcButton title={"+/-"} color="gray" />
                    <CalcButton
                        title={"%"}
                        color="gray"
                        onPress={() => {
                            if (/^\d+$/.test(calcString.substring(-1))) setCalcString(calcString + "%");
                        }}
                    />

                    <CalcButton title={"÷"} color="blue" onPress={() => onOperationPress("÷")} />
                    <CalcButton title={7} color="onyx" onPress={() => onNumberPress(7)} />
                    <CalcButton title={8} color="onyx" onPress={() => onNumberPress(8)} />
                    <CalcButton title={9} color="onyx" onPress={() => onNumberPress(9)} />
                    <CalcButton title={"×"} color="blue" onPress={() => onOperationPress("*")} />
                    <CalcButton title={4} color="onyx" onPress={() => onNumberPress(4)} />
                    <CalcButton title={5} color="onyx" onPress={() => onNumberPress(5)} />
                    <CalcButton title={6} color="onyx" onPress={() => onNumberPress(6)} />
                    <CalcButton title={"-"} color="blue" onPress={() => onOperationPress("-")} />
                    <CalcButton title={1} color="onyx" onPress={() => onNumberPress(1)} />
                    <CalcButton title={2} color="onyx" onPress={() => onNumberPress(2)} />
                    <CalcButton title={3} color="onyx" onPress={() => onNumberPress(3)} />
                    <CalcButton title={"+"} color="blue" onPress={() => onOperationPress("+")} />
                    <CalcButton
                        title={"."}
                        color="onyx"
                        onPress={() => {
                            let tempCalcString = calcString.split(" ");
                            if (!tempCalcString[tempCalcString.length - 1].includes("."))
                                setCalcString(calcString + `.`);
                        }}
                    />
                    <CalcButton title={0} color="onyx" onPress={() => onNumberPress(0)} />
                    <CalcButton
                        title={""}
                        icon={
                            <Image
                                source={require("../../assets/history.png")}
                                width={48}
                                style={{
                                    alignSelf: "center",
                                    width: 48,
                                }}
                                blurRadius={0}
                            />
                        }
                        color="onyx"
                        onPress={() => {
                            navigation.navigate("History");
                        }}
                    />
                    <CalcButton
                        title={"="}
                        color="blue"
                        onPress={async () => {
                            let tempValue = value;
                            let tempString = calcString;
                            setCalcString("");
                            setValue(0);

                            await axios.post(`${BASE_URL}/${user.uuid}/transaction`, {
                                calculation_string: tempString + " = " + tempValue,
                            });
                        }}
                    />
                </View>
            </View>
        </View>
    );
};

const calcStyling = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 16,
        height: "100%",
        position: "relative",
        alignItems: "flex-start",
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "flex-start",
    },
    specialButton: {
        borderRadius: 15,
        margin: 1,
        width: "24.2%",
        aspectRatio: "1/1",
        display: "flex",
        justifyContent: "center",
        alignContent: "center",
        backgroundColor: "#3B3D43",
    },
});

export default Calculator;
