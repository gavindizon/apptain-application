import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: "100%",
        position: "relative",
        backgroundColor: "#000",
        color: "#fff",
        alignItems: "flex-end",
        justifyContent: "flex-end",
    },
    innerContainer: {
        height: "95%",
        width: "100%",
        backgroundColor: "#292A2D",
        borderTopLeftRadius: "25px",
        borderTopRightRadius: "25px",
        display: "flex",
        position: "relative",
    },
});
