import React from "react";
import { Pressable, Text, StyleSheet } from "react-native";

const CalcButton = ({ title, onPress, color = "gray", icon }) => {
    return (
        <Pressable onPress={onPress} style={{ ...styles[color], ...styles.container }}>
            {icon ? (
                icon
            ) : (
                <Text
                    style={{
                        color: "#FFF",
                        fontSize: 44,
                        textAlign: "center",
                        textAlignVertical: "center",
                        fontWeight: "600",
                    }}
                >
                    {title}
                </Text>
            )}
        </Pressable>
    );
};

export const styles = StyleSheet.create({
    container: {
        borderRadius: 15,
        margin: 1,
        width: "24.2%",
        aspectRatio: "1/1",
        display: "flex",
        justifyContent: "center",
        alignContent: "center",
        textAlign: "center",
    },
    blue: {
        backgroundColor: "#3764B4",
    },
    gray: {
        backgroundColor: "#5B5E67",
    },
    onyx: {
        backgroundColor: "#3B3D43",
    },
});

export default CalcButton;
