import React from "react";
import { Text, View } from "react-native";

const HistoryLog = ({ calcString, answer }) => {
    return (
        <View style={{ backgroundColor: "#4D5057", height: 95, width: "100%", marginBottom: 2, padding: 24 }}>
            <Text style={{ fontSize: 20, fontWeight: "600", color: "#FFF", marginBottom: 4 }}>{calcString}</Text>
            <Text style={{ fontSize: 20, fontWeight: "600", color: "#FFF" }}>= {answer}</Text>
        </View>
    );
};

export default HistoryLog;
